﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FulrodStoreApi.Models
{
    public class Product
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        [Required]
        public int Category { get; set; }
        [Required]
        public int Price { get; set; }
        public bool Promotion { get; set; }
        public double Discount { get; set; }
        [Required]
        [Range(1, 1000)]
        public int Quantity { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public DateTime ModifiedDate { get; set; }
    }
}
